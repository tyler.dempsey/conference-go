import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def find_image(city, state):

    # authorization header:
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"

    # use requests to get an image:
    params = {"query": city + " " + state, "per_page": 1}
    response = requests.get(url, headers=headers, params=params)

    content = json.loads(response.content)

    # # create a dictionary of data to use:
    # image = {
    #   "photo_url": content["photos"][0]["src"]["original"],
    #   "photographer": content["photos"][0]["photographer"]
    # }

    try:
        return {"photo": content["photos"][0]["src"]["original"]}
    except KeyError:
        return {"photo": None}


def get_weather_data(city, state):

    # GETTING GEO DATA
    url = "http://api.openweathermap.org/geo/1.0/direct"
    code = "USA"
    # use params to get geo data:
    params = {
        "q": city + ", " + state + ", " + code,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    # # create a dictionary of data to use:
    print("@@@@", content)

    try:

        lat = content[0]["lat"]
        lon = content[0]["lon"]

        # print("@@@@@", lat, lon)

        # GETTING WEATHER DATA
        url = "https://api.openweathermap.org/data/2.5/weather"
        params2 = {
            "lat": lat,
            "lon": lon,
            "appid": OPEN_WEATHER_API_KEY,
            "units": "imperial",
        }
        response2 = requests.get(url, params=params2)

        content2 = json.loads(response2.content)

        # print("@@@@", content2)
        weather = {
            "temp": int(content2["main"]["temp"]),
            "description": content2["weather"][0]["description"],
        }

        return weather
    except:
        return None
